import sys
import pygmo as pg
from matplotlib import pyplot as plt
from user_defined_problem import UserDefinedProblem

base_individual = [
    64,       # win_size
    8,        # block_size
    4,        # block_stride
    4,        # cell_size
    9,        # n_bins
    1,        # deriv_aperture
    4.0,      # win_sigma
    0,        # 'histogram_norm_type'
    2.0e-01,  # 'l2_hys_threshold'
    0,        # 'gamma_correction'
    64,       # 'n_levels'
    0,        # 'signed_gradient'
    100.0,    # 'C'
    2,        # 'kernel'
    3,        # 'degree'
    0.009,    # 'gamma'
    0.0,      # 'coef0'
    1,        # 'shrinking'
    1e-09,    # 'tol'
    1,        # 'class_weight'
    0,        # 'decision_function_shape'
    5         # 'n_folds'
]

pg.set_global_rng_seed(seed=324)
udp = UserDefinedProblem(sys.argv[1])
prob = pg.problem(udp)

uda = pg.sade(gen=500, variant=14, variant_adptv=2, seed=342)
algo = pg.algorithm(uda)

algo.set_verbosity(1)
pop = pg.population(prob, 499)
pop.push_back(base_individual)
algo.evolve(pop)

evolution_log = algo.extract(type(uda)).get_log()
final_result = (evolution_log[-1][2])

plt.plot(evolution_log[:, 1], evolution_log[:, 2], label=algo.get_name())
plt.legend()
plt.grid()
plt.title('Evolution Score')
plt.ylabel('- Score')
plt.xlabel('Evaluations')
plt.savefig(udp.log_dir + 'evolution_score.png')
plt.gcf().clear()
print('Final Result:', final_result)
