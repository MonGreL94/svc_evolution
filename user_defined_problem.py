import pickle
import cv2 as cv
import numpy as np
# from sklearn.svm import SVC
from thundersvm.python.thundersvm.thundersvm import SVC
from sklearn.model_selection import cross_validate


class UserDefinedProblem:

    def __init__(self, log_dir):
        self.log_dir = log_dir
        self.X_img, self.y, self.Xt_img, self.yt = UserDefinedProblem._load_data_set(self.log_dir.split('/')[-2])
        with open(self.log_dir + '/report.txt', 'w') as report_file:
            report_file.write('')
        self.n_eval = 1
        self.kernel_type = {0: 'linear', 1: 'polynomial', 2: 'rbf'}
        self.class_weight_type = {0: None, 1: 'balanced'}
        self.decision_function_shape_type = {0: 'ovo', 1: 'ovr'}
        self.hog_param_names = ('win_size', 'block_size', 'block_stride', 'cell_size', 'n_bins', 'deriv_aperture',
                                'win_sigma', 'histogram_norm_type', 'l2_hys_threshold', 'gamma_correction', 'n_levels',
                                'signed_gradient')
        self.svm_param_names = ('C', 'kernel', 'degree', 'gamma', 'coef0', 'shrinking', 'probability', 'tol',
                                'class_weight', 'n_jobs', 'decision_function_shape', 'n_folds')
        print('Dataset Loaded!', self.log_dir)

    @staticmethod
    def _load_data_set_dict(object_type):
        with open(object_type + '_train_set.pkl', 'rb') as f_train:
            train_set_dict = pickle.load(f_train)
        with open(object_type + '_test_set.pkl', 'rb') as f_test:
            test_set_dict = pickle.load(f_test)
        return train_set_dict, test_set_dict

    @staticmethod
    def _load_data_set_arrays(data_set_dict, object_type):
        X_img = []
        y = []
        for image_name, image_class in data_set_dict.items():
            X_img.append(cv.imread('cropped_data_set/' + object_type + '/' + image_name, 0))
            y.append(image_class)
        y = np.array(y)
        return X_img, y

    @staticmethod
    def _load_data_set(object_type):
        train_set_dict, test_set_dict = UserDefinedProblem._load_data_set_dict(object_type)
        X_img, y = UserDefinedProblem._load_data_set_arrays(train_set_dict, object_type)
        Xt_img, yt = UserDefinedProblem._load_data_set_arrays(test_set_dict, object_type)
        return X_img, y, Xt_img, yt

    @staticmethod
    def _extract_hog_features_circle(X_img, hog_params):
        win_size = hog_params[0]
        block_size = hog_params[1]
        block_stride = hog_params[2]
        cell_size = hog_params[3]
        n_bins = hog_params[4]
        deriv_aperture = hog_params[5]
        win_sigma = hog_params[6]
        histogram_norm_type = hog_params[7]
        l2_hys_threshold = hog_params[8]
        gamma_correction = hog_params[9]
        n_levels = hog_params[10]
        signed_gradient = hog_params[11]

        hog = cv.HOGDescriptor(win_size, block_size, block_stride, cell_size, n_bins, deriv_aperture, win_sigma,
                               histogram_norm_type, l2_hys_threshold, gamma_correction, n_levels, signed_gradient)

        output_dim = int(((win_size[0]-block_size[0])/block_stride[0]+1)**2 * n_bins * (block_size[0]/cell_size[0])**2)

        Xf = np.zeros((X_img.shape[0], output_dim))

        for i in range(X_img.shape[0]):
            img = X_img[i]
            Xf[i, :] = hog.compute(img)[:, 0]

        return Xf, output_dim

    def fitness(self, individual):

        print('Evaluation Number:', self.n_eval)

        win_size = (int(round(individual[0])), int(round(individual[0])))
        block_size = (int(round(individual[1])), int(round(individual[1])))
        block_stride = (int(round(individual[2])), int(round(individual[2])))
        cell_size = (int(round(individual[3])), int(round(individual[3])))

        if (win_size[0] - block_size[0]) % block_stride[0] == 0 and block_size[0] % cell_size[0] == 0:

            n_bins = int(round(individual[4]))
            deriv_aperture = int(round(individual[5]))
            win_sigma = individual[6]
            histogram_norm_type = int(round(individual[7]))
            l2_hys_threshold = individual[8]
            gamma_correction = bool(round(individual[9]))
            n_levels = int(round(individual[10]))
            signed_gradient = bool(round(individual[11]))

            if int(((win_size[0] - block_size[0]) / block_stride[0] + 1) ** 2 * n_bins * (block_size[0] / cell_size[0])
                   ** 2) > 8100:
                print('Feature vector too big!')
                score = 0.0
            else:
                hog_params = (win_size, block_size, block_stride, cell_size, n_bins, deriv_aperture, win_sigma,
                              histogram_norm_type, l2_hys_threshold, gamma_correction, n_levels, signed_gradient)

                X_img_resized = []
                for image in self.X_img:
                    X_img_resized.append(cv.resize(image, win_size, interpolation=cv.INTER_CUBIC))

                X_img_resized = np.array(X_img_resized)
                X, _ = UserDefinedProblem._extract_hog_features_circle(X_img_resized, hog_params)
                print(X_img_resized.shape, X.shape, self.y.shape)

                C = individual[12]
                kernel = self.kernel_type[int(round(individual[13]))]
                degree = int(round(individual[14]))
                gamma = individual[15]
                coef0 = individual[16]
                shrinking = bool(round(individual[17]))
                tol = individual[18]
                class_weight = self.class_weight_type[int(round(individual[19]))]
                decision_function_shape = self.decision_function_shape_type[int(round(individual[20]))]
                n_folds = int(round(individual[21]))

                svm_params = (C, kernel, degree, gamma, coef0, shrinking, True, tol, class_weight, -1,
                              decision_function_shape, n_folds)

                print('Start fitting!', svm_params)

                svm = SVC(C=C, kernel=kernel, degree=degree, gamma=gamma, coef0=coef0, shrinking=shrinking,
                          probability=True, tol=tol, class_weight=class_weight, n_jobs=-1,
                          decision_function_shape=decision_function_shape)

                fit_dict = cross_validate(svm, X, self.y, cv=n_folds, verbose=True, return_train_score=True,
                                          return_estimator=True)

                fitted_svm = fit_dict['estimator'][fit_dict['test_score'].argmax()]

                Xt_img_resized = []
                for image in self.Xt_img:
                    Xt_img_resized.append(cv.resize(image, win_size, interpolation=cv.INTER_CUBIC))

                Xt_img_resized = np.array(Xt_img_resized)
                Xt, _ = UserDefinedProblem._extract_hog_features_circle(Xt_img_resized, hog_params)
                print(Xt_img_resized.shape, Xt.shape, self.yt.shape)

                print('Start Testing!')
                score = fitted_svm.score(Xt, self.yt)

                svm_params_string = ''
                for j in range(len(svm_params)):
                    if j != 14:
                        svm_params_string += self.svm_param_names[j] + ': ' + str(svm_params[j]) + ', '
                    else:
                        svm_params_string += self.svm_param_names[j] + ': ' + str(svm_params[j])

                hog_params_string = ''
                for i in range(len(hog_params)):
                    if i != 11:
                        hog_params_string += self.hog_param_names[i] + ': ' + str(hog_params[i]) + ', '
                    else:
                        hog_params_string += self.hog_param_names[i] + ': ' + str(hog_params[i])

                svm_model_file_name = 'score_' + str(score) + '_n_eval_' + str(self.n_eval) + '.pkl'

                with open(self.log_dir + 'report.txt', 'a') as report_file:
                    report_file.write('SVC params --> ' + svm_params_string + '\n' +
                                      'HOG params --> ' + hog_params_string + '\n' +
                                      'Train scores --> ' + str(fit_dict['train_score']) + '\n' +
                                      'Val scores --> ' + str(fit_dict['test_score']) + '\n' +
                                      svm_model_file_name + '\n' + str(score) + '\n\n')

                fitted_svm.save_to_file(self.log_dir + svm_model_file_name)

                del hog_params
                del X_img_resized
                del X
                del C
                del kernel
                del degree
                del gamma
                del coef0
                del shrinking
                del tol
                del class_weight
                del decision_function_shape
                del n_folds
                del svm_params
                del svm
                del fit_dict
                del fitted_svm
                del Xt_img_resized
                del Xt
                del svm_params_string
                del hog_params_string
                del report_file

            del n_bins
            del deriv_aperture
            del win_sigma
            del histogram_norm_type
            del l2_hys_threshold
            del gamma_correction
            del n_levels
            del signed_gradient

        else:
            print('Incorrectly HOG params sizes!')
            score = 0.0

        del win_size
        del block_size
        del block_stride
        del cell_size

        self.n_eval += 1

        return [-score]

    def get_bounds(self):

        lower_bounds = [
            8,        # win_size
            4,        # block_size
            1,        # block_stride
            1,        # cell_size
            1,        # n_bins
            0,        # deriv_aperture
            -10.0,    # win_sigma
            0,        # 'histogram_norm_type'
            0.0,      # 'l2_hys_threshold'
            0,        # 'gamma_correction'
            1,        # 'n_levels'
            0,        # 'signed_gradient'
            0.0,      # 'C'
            -0.5,     # 'kernel'
            0,        # 'degree'
            1e-09,    # 'gamma'
            0.0,      # 'coef0'
            0,        # 'shrinking'
            1e-09,    # 'tol'
            0,        # 'class_weight'
            0,        # 'decision_function_shape'
            3         # 'n_folds'
        ]

        upper_bounds = [
            80,       # win_size
            16,       # block_size
            16,       # block_stride
            16,       # cell_size
            20,       # n_bins
            10,       # deriv_aperture
            10.0,     # win_sigma
            5,        # 'histogram_norm_type'
            5.0,      # 'l2_hys_threshold'
            1,        # 'gamma_correction'
            100,      # 'n_levels'
            1,        # 'signed_gradient'
            700.0,    # 'C'
            2.5,      # 'kernel'
            6,        # 'degree'
            1e-03,    # 'gamma'
            100.0,    # 'coef0'
            1,        # 'shrinking'
            1e-01,    # 'tol'
            1,        # 'class_weight'
            1,        # 'decision_function_shape'
            6         # 'n_folds'
        ]

        return lower_bounds, upper_bounds
