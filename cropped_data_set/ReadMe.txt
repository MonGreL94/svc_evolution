class_id_circle = {'one': 0, 'two': 1, 'three': 2, 'four': 3, 'five': 4, 'six': 5, 'arrow_left': 6, 'arrow_right': 7, 'arrow_up': 8, 'denied': 9}

class_id_rectangle = {'0_degree': 0, '45_degree': 1, '90_degree': 2, '180_degree': 3}